// ------------------------------------ game
var Game = function (data) {
  this.body = $('body');
  this.el = $('#slide');
  this.shops = data.shops;
  this.$document = $(document);
  this.money = [1,2,5,10,20,50];
  var click = ('ontouchstart' in document.documentElement) ? 'touchstart' : 'click';

  if (!localStorage.currentLevel) {
    localStorage.currentLevel = 1;
 //   $.colorbox({href: "#setLevel", inline: true, width: "50%", height: "auto", closeButton: false, overlayClose: false, escKey: false});
  }
  this.currentLevel = parseInt( localStorage.currentLevel );
  this.gameResult = {
    shop_time: 0,
    shop_errors: 0,
    product_time:0,
    product_errors_missed: 0,
    product_errors_wrong: 0,
    product_errors_extra: 0,
    level: this.currentLevel
      };
  // выбираем текущий магазин
  this.shop = _(this.shops).shuffle()[0];

  this.shop.requiredProducts = setLevel(this.shop, this.currentLevel);
  this.shop.sum = 0;
  for (var i = 0; i <this.shop.requiredProducts.length; i++)
      this.shop.sum += this.shop.requiredProducts[i].price * this.shop.requiredProducts[i].isRequired;

  //    початковий перелік прдуктів
  this.screen = this.getTmpl("task", {
    "requiredProducts": this.shop.requiredProducts,
    "lvl": this.currentLevel
  },this);
  // засікаємо час
  this.tempTime = new Date();
/*
* Звук
*/
  this.audio = {
    wrongItem: $('<audio src="audio/wrong_item.mp3">').appendTo('body').get(0),
    missed: $('<audio src="audio/missed_cart.mp3">').appendTo('body').get(0),
    extra: $('<audio src="audio/extra.mp3">').appendTo('body').get(0),
    chooseShop: $('<audio src="audio/choose_shop.mp3">').appendTo('body').get(0),
    wrongShop: $('<audio src="audio/wrong_shop.mp3">').appendTo('body').get(0)
  };
  // клік по першому списку
  this.el.on(click, ".checkList", _.bind(this.clickCheckList, this));

  // клики по магазинам
  this.el.on(click, ".shop", _.bind(this.clickShop, this));

  // клики по товарам
//  this.el.on("mousedown touchstart", ".prod", _.bind(this.dragNDrop, this));
//  this.el.on("touchstart", ".prod", _.bind(this.dragNDrop, this));
  this.el.on(click, ".items li", _.bind(this.checkInList, this));

  this.el.on(click, "#done", _.bind(this.checkDone, this));

  // клік по чеку
  this.el.on("mousedown touchstart", ".money", _.bind(this.dragNDrop, this));
  this.el.on(click, "#goHome", _.bind(this.clickPay, this));

//  this.body.on("dragstart", "*", function (ev) {
//    ev.preventDefault();
//    ev.stopPropagation();
//  });
  this.body.on("touchmove","*", function (ev) {
    ev.preventDefault();
  });
};


Game.prototype = _.extend(Game.prototype, {
  destroy: function () {
    this.screen.off();
    this.screen.remove();
  },
  clickCheckList: function (ev) {
    this.destroy();
    if (this.currentLevel < 3) this.audio.chooseShop.play();
    this.screen = this.getTmpl("choose", {
      "shops": _(this.shops).map(function (shop) {
        return {
          'id': shop.id,
          'name': shop.name
        }
      }),

      "products": _(this.shop.requiredProducts).shuffle()
    }, this);

  },
checkDone: function(ev){
  if (!this.checkAllTaken()) {
    alert('Ти ще не все взяв');
  }
},
  clickShop: function (ev) {
    ev.preventDefault();
    ev.stopPropagation();

    var shop = $(ev.currentTarget);

    if (shop.data('id') === this.shop.id) {
      this.gameResult.shop_time = new Date() - this.tempTime;
      this.gameResult.shop_type = this.shop.id;

      this.destroy();

      this.screen = this.getTmpl("shop", {
        "products": this.shop.products,
        "requiredProducts": this.shop.requiredProducts,
        "lvl": this.currentLevel
      });
        $('.prod').draggable();
      this.tempTime = new Date();
    } else {
      this.gameResult.shop_errors += 1;
      if (this.currentLevel < 3) this.audio.wrongShop.play();
      alert('В цьому магазині нема потрібних товарів');
    }
  },


  /**
   * работа с товарами
   * */
  getProduct: function (id) {
    return _(this.shop.products).find(function (product) {
      return product.id === id;
    });
  },
  takeProduct: function (id, $drag) {
    var product = this.getProduct(id);

    this.toThumbs($drag, $('#cart'));
    if (product.taken) product.taken +=1
    else product.taken = 1;

    if (product.taken == product.isRequired){
      if (this.currentLevel < 3) this.screen.find(".item#id-" + id).wrapInner($('<strike>'))
      else this.checkProduct = id;
    }

    if (this.currentLevel === 1) this.checkAllTaken();
  },
  toThumbs: function($drag, $where) {
    var img = $drag.clone().css({ position: 'static' });
    img.appendTo($where);
    $drag.remove();
  },
  checkAllTaken: function () {
    var isAllTaken = _(this.shop.products).every(function (product) {
      return (product.taken === product.isRequired) || !product.isRequired
    });

    if (isAllTaken) {
      this.gameResult.product_time = new Date() - this.tempTime;
      this.destroy();

      this.screen = this.getTmpl("pay", {
        "requiredProducts": this.shop.requiredProducts,
        "shop": this.shop,
        "lvl": this.currentLevel,
        "money": this.money
      });
    }
    return isAllTaken;
  },
  checkInList: function (ev) {
    var elem = ev.currentTarget;
    if (this.checkProduct) {
      if (elem.id == 'id-'+this.checkProduct) {
        $(elem).wrapInner($('<strike>'));
        this.checkProduct = '';
      } else alert('Ти взяв інший продукт');
          }
  },
  dragNDrop: function (ev) {
    ev.preventDefault();
    var dragEv = ev.originalEvent;

    var $product = $(ev.currentTarget),
        offset = $product.offset(),
        shift = {
          left: dragEv.pageX - offset.left,
          top: dragEv.pageY - offset.top
        },
        $drag = $product.clone().appendTo('body').css($.extend({
          position: "absolute",
          zIndex: 100
        }, offset));

    this.$document.on({
      'mousemove.product': _.bind(startMove, this),
      'mouseup.product': _.bind(endMove, this),
      'touchmove.product': _.bind(startMove,this),
      'touchend.product': _.bind(endMove,this)
    });

    function startMove(ev) {
      ev = ev.originalEvent;
      $drag.css({
        left: ev.pageX - shift.left,
        top: ev.pageY - shift.top
      });
    }
    function endMove(ev) {
      this.$document.off('mousemove.product');
      this.$document.off('mouseup.product');
      this.$document.off('touchmove.product');
      this.$document.off('touchend.product');

      if (this.checkDrag($drag)) {
        $drag.animate($product.offset(), {duration: 500, complete: function () {
          $drag.remove();
        }});
      }
    }
  },
  checkDrag: function($drag) {
    if ($drag.hasClass('prod')) {
      var dragOpt = getFullOpt($drag),
          cartOpt = getFullOpt($('#cart')),
          id = $drag.data('id'),
          product = this.getProduct(id);

      if (isAppear(dragOpt, cartOpt)) {
        if (this.checkProduct) {
          alert('спочатку відміть попередній продукт у списку');
        } else if (!product.isRequired) {
          this.gameResult.product_errors_wrong += 1;
          if (this.currentLevel < 3) this.audio.wrongItem.play();
          alert('Перевір свій список');
        } else if (product.taken === product.isRequired) {
          this.gameResult.product_errors_extra += 1;
          if (this.currentLevel < 3) this.audio.extra.play();
          alert('Ти це вже взяв');
        } else {
          this.takeProduct(id, $drag)
        }
      } else {
        if (this.currentLevel < 3) this.audio.missed.play();
        this.gameResult.product_errors_missed += 1;
      }
      ;

      return !isAppear(dragOpt, cartOpt) || !product.isRequired || product.taken || this.checkProduct;
    } else if ($drag.hasClass('money')) {
      var nom = $drag.data('nom'),
          dragOpt = getFullOpt($drag),
          cashboxOpt = getFullOpt($('.pay'));
      if (isAppear(dragOpt,cashboxOpt)) {
        if (!this.moneyAmount) {
          this.toThumbs($drag, $('div.pay'));
          this.moneyAmount = nom;
        } else if (this.moneyAmount < this.shop.sum) {
          this.moneyAmount += nom;
          this.toThumbs($drag, $('div.pay'));
        } else {
          alert('Ти Даєш забагато грошей');
        }
      } else {
        alert('Давай гроші продавцю');
      }
      return !isAppear(dragOpt, cashboxOpt)||(this.moneyAmount - nom > this.shop.sum);
    }
  },

  /**
   * end: работа с товарами
   * */


  clickPay: function (ev) {
    if (this.currentLevel === 1) {
      alert('Молодець, ти виграв');
      this.gameOver();
    } else if (!this.moneyAmount){
      alert('Заплати за продукти');
    } else if (this.shop.sum < this.moneyAmount) {
      alert('Візьміть вашу решту: ' + (this.moneyAmount - this.shop.sum).toFixed(2) + ' грн.');
      this.gameOver();
    } else {
      alert('Ти дав недостатньо грошей');
    }

  },
  gameOver: function() {
    for (var key in this.gameResult) {
      if (!localStorage[key]) localStorage[key] = this.gameResult[key]
      else localStorage[key] += ','+this.gameResult[key];
    };
    location.reload();
  },
  getTmpl: function (index, data, where) {
    data = _(data).extend({shop: this.shop});
    return $(tmpl('tmpl_' + index, data)).appendTo(this.el);
  }
});