/*$(function(){
	var $body = $('body'),
			$window = $(window),
			$document = $(document);

	window.getCoords = function($el) {
		while ($el.css('position') === 'static') {
			$el = $el.parent();
		}

		var offset = $el.offset();

		return {
			top: Math.round(offset.top + $window.scrollTop()),
			left: Math.round(offset.left + $window.scrollLeft())
		}
	}
});*/



function isAppear(drag, goal) {
	return drag.right > goal.left && drag.left < goal.right && drag.top < goal.bottom && drag.bottom > goal.top;
}

function getFullOpt(obj) {
	var offset = obj.offset();

	return $.extend(offset, {
		right: offset.left + obj.width(),
		bottom: offset.top + obj.height()
	});
}

function getMax(arr) {
  var max = 0;
  for (var i = 0; i<arr.length; i++) {
    for (var j = 0; j < arr[i].length; j++) {
      max = arr[i][j] > max ? arr[i][j] : max;
    }
  }

  return max;
}

function play(str) {
  $('<audio src="audio/'+str+'.mp3" autoplay>').appendTo('body').on('ended', function(){$(this).remove()});
}

function setLevel(shop, lvl) {
  shop.products = _.shuffle(shop.products);
  for (var i = 0; i < shop.products.length; i++) {
    switch (lvl) {
      case 1:
        shop.products[i].isRequired = i < 3 ? 1 : 0;
        break;
      case 2:
        shop.products[i].isRequired = i < 3 ? i==1 ? 2 : 1 : 0;
        break;
      case 3:
        shop.products[i].isRequired = i < 5 ? i < 3 ? i == 1 ? 3 : 2 : 1 : 0;
        break;
    }
  }

  shop.products = _.shuffle(shop.products);

  return _(shop.products).filter(function(prod) {
    return prod.isRequired;
  });
}