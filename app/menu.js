/**
 * Created by Роман on 28.04.2014.
 */

$( function() {
      $('.menubutton').colorbox({inline:true, width:"30%", closeButton: false});
      $('.setLevel').colorbox({inline: true, width: "50%", height: "auto"});
      $('.closeMenu').on("click", $.colorbox.close);
      $('.level').on("click",function(ev) {
        lvl = $(ev.currentTarget).data('lvl');
        localStorage['currentLevel'] = lvl;
        location.reload();
      });
      $('.clearHistory').on("click",function () {
        localStorage.clear();
        location.reload();
      });
    }
);