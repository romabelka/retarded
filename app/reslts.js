/**
 * Created by Роман on 22.04.2014.
 */

var results = {},
    graphs = [
      {
        id: 'time',
        y: ['shop_time', 'product_time'],
        legend: ['Час витрачений на вибір магазину', 'Час витрачений на вибір продуктів'],
        colors: ['green', 'red']
      },
      {
        id: 'shop_errors',
        y: ['shop_errors'],
        legend: ['Помилки при виборі магазину'],
        colors: ['yellow']
      },
      {
        id: 'product_errors',
        y: ['product_errors_wrong','product_errors_extra','product_errors_missed'],
        legend: ['Взяв не той товар','Взяв більше продуктів ніж треба','Промахнувся повз кошик'],
        colors: ['red','brown','green']
      }
    ];


for (var key in localStorage) {
  results[key] = localStorage[key].split(',');
}

$(function () {
  $(tmpl('tmpl_graphs', graphs)).appendTo($('#page'));
  for (var i = 0; i < graphs.length; i++)   drawGraph(graphs[i]);
});
//малюємо графіки

function drawGraph(graph) {
  var canv = $('#' + graph.id + ' > canvas').get(0),
      ul = $('#' + graph.id + ' > ul');
      ctx = canv.getContext('2d');
  var startX = 20,
      maxY = 20,
      startY = canv.height - 20,
      maxX = canv.width - 20;
//переводимо значення в потрібний числовий формат
  var y = [];
  for (var j = 0; j < graph.y.length; j++) {
    y[j] = [];
    for (var i = 0; i < results[graph.y[j]].length; i++) {
      if (graph.y[j].indexOf('time') != -1) y[j][i] = Math.round(parseInt(results[graph.y[j]][i]) / 1000)
      else y[j][i] = parseInt(results[graph.y[j]][i]);
    }
  }
  var scaleX = Math.round((maxX - startX) / results[graph.y[0]].length),
      scaleY = Math.round((maxY - startY) / getMax(y));

//малюємо осі
  ctx.beginPath();
  ctx.moveTo(startX, startY);
  ctx.lineTo(maxX, startY);
  ctx.moveTo(startX, startY)
  ctx.lineTo(startX, maxY);
  ctx.stroke(); // *22
// малюємо графіки

  for (var i = 0; i < y.length; i++) {
    ctx.strokeStyle = graph.colors[i];
    ctx.beginPath()
    ctx.moveTo(startX, startY+y[i][0]*scaleY);
    for (var j = 1; j<y[i].length; j++) {
      ctx.lineTo(startX+j*scaleX,startY+y[i][j]*scaleY);
    }
    ctx.stroke();
    var li = (ul.get(0).appendChild(document.createElement('li')));
    li.innerHTML = graph.legend[i];
    li.style.color = graph.colors[i];
  }
}

function drawChart() {
  var data = google.visualization.arrayToDataTable([
    ['Year', 'Sales', 'Expenses'],
    ['2004',  1000,      400],
    ['2005',  1170,      460],
    ['2006',  660,       1120],
    ['2007',  1030,      540]
  ]);

  var options = {
    title: 'Company Performance'
  };

  var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
  chart.draw(data, options);
}
