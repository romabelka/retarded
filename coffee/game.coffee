class Game
  money = [1,2,5,10,20,50]
  $document = $(document)
  click = 'click'
  if ('ontouchstart' in document.documentElement) then click = 'touchstart'
  audio =
    wrongItem: $('<audio src="audio/wrong_item.mp3">').appendTo('body').get(0)
    missed: $('<audio src="audio/missed_cart.mp3">').appendTo('body').get(0)
    extra: $('<audio src="audio/extra.mp3">').appendTo('body').get(0)
    chooseShop: $('<audio src="audio/choose_shop.mp3">').appendTo('body').get(0)
    wrongShop: $('<audio src="audio/wrong_shop.mp3">').appendTo('body').get(0)

  constructor: (data) ->
    @body = $('body')
    @el = $('#slide')
    @shops = data.shops

    localStorage.currentLevel = 1 unless localStorage.currentLevel
    @currentLevel = parseInt localStorage.currentLevel
    @gameResult =
      shop_time: 0
      shop_errors: 0
      product_time:0
      product_errors_missed: 0
      product_errors_wrong: 0
      product_errors_extra: 0
      level: @currentLevel

    @shop = _(@shops).shuffle()[0]
    @shop.requiredProducts = setLevel @shop, @currentLevel
    @shop.sum = 0

    @shop.sum += product.price * product.isRequired for product in @shop.requiredProducts
    @screen = @getTmpl("task", {
      'requiredProducts': @shop.requiredProducts
      'lvl': @currentLevel
    }, @)

    @tempTime = new Date()
    @el
      .on click, '.checkList', @clickCheckList
      .on click, '.shop', @clickShop
      .on click, '.items li', @checkInList
      .on click, '#done', @checkDone
      .on click, '#goHome', @clickPay
  destroy: ->
    do @screen.off
    do @screen.remove
  clickCheckList: (ev) =>
    do @destroy
    if @curentLevel < 3 then do @audio.chooseShop.play
    @screen = @getTmpl("choose"
      "shops": _(@shops).map (shop) ->
        {
           "id": shop.id
          "name": shop.name
        }
      "products": _(@shop.requiredProducts).shuffle()
    @)
  checkDone: (ev) =>
    unless @checkAllTaken() then alert "Ти ще не все взяв"
  clickShop: (ev) =>
    do ev.preventDefault
    do ev.stopPropagation
    shop = $ ev.currentTarget
    console.log shop.data('id'), @shop.id
    if shop.data("id") == @shop.id
      @gameResult.shop_time = new Date() - @tempTime
      @gameResult.shop_type = @shop.id

      do @destroy

      @screen = @getTmpl "shop",
        "products": @shop.products,
        "requiredProducts": @shop.requiredProducts,
        "lvl": @currentLevel
      @chooseProd @
    else
      @gameResult.shop_errors += 1
      if @currentLevel < 3 then do @audio.wrongShop.play
      alert('В цьому магазині нема потрібних товарів')
  chooseProd: (self) ->
    $('.prod').draggable
      containment: @screen
      helper: 'clone'
      scroll: false
      zIndex: 100
    $('.prod').on 'dragstart', (ev, ui) =>
      el = $(ev.currentTarget)
      prod = @getProduct el.data 'id'
      rev = true
      prod.taken = 0 unless prod.taken
      if prod.isRequired && prod.taken < prod.isRequired then rev = 'invalid'
      el.draggable 'option', 'revert', rev
    $('#cart').droppable
      drop: (ev, ui) =>
        prod = @getProduct ui.draggable.data("id")
        console.log prod.taken, prod.isRequired, @checkProduct
        unless prod.isRequired
          alert "Перевір свій список"
        else if prod.taken == prod.isRequired
          alert "Ти це вже взяв"
        else if (!@checkProduct)
          @takeProduct ui.draggable
          if @currentLevel < 3 && prod.taken == prod.isRequired
            @screen.find(".item#id-#{prod.id}").wrapInner($ '<strike>')
          if @currentLevel < 3 then do @checkAllTaken
        else
          alert 'Спочатку відміть попередній продукт'
    @tempTime = new Date()
  checkAllTaken: ->
    isAllTaken = _(@shop.products).every (prod) ->
      (prod.taken == prod.isRequired) || !prod.isRequired
    if isAllTaken
      do @destroy
      @screen = @getTmpl 'pay',
        'requiredProducts': @shop.requiredProducts
        'shop': @shop
        'lvl': @currentLevel
        'money': money
      do @payCheck
    isAllTaken
  payCheck: ->
    @cash = 0
    $('.money').draggable
      helper: 'clone'
      scroll: false
      revert: 'invalid'
      zIndex: 100
    $('.pay').droppable
      drop: (ev, ui) =>
        @cash += ui.draggable.data 'nom'
        ui.draggable.clone().hide().appendTo('div.pay').fadeIn(300)
    $('.money').on 'dragstart', (ev, ui) =>
      if @shop.sum < @cash then $('.pay').droppable 'disable'
  clickPay: =>
    if @currentLevel ==1
      alert 'Молодець, ти виграв'
      do @gameOver
    else if !@cash
      alert 'Заплати за продукти'
    else if @shop.sum <@cash
      alert "Візьміть вашу решту: #{(@cash - @shop.sum).toFixed 2} грн."
      do @gameOver
    else
      alert "Ти дав недостатньо грошей"
  takeProduct: (obj) ->
     prod = @getProduct obj.clone().hide().appendTo('#cart').fadeIn(300).data 'id'
     if prod.taken then prod.taken+=1 else prod.taken = 1
     if @currentLevel == 3 && prod.taken == prod.isRequired then @checkProduct = prod.id
  checkInList: (ev) =>
    el = ev.currentTarget
    if @checkProduct
      if el.id == "id-#{@checkProduct}"
        $(el).wrapInner($ '<strike>')
        @checkProduct = ''
      else alert 'Ти взяв інший продукт'
  gameOver: ->
    alert 'ok'
  getProduct: (id) ->
    _(@.shop.products).find (prod) -> prod.id == id
  getTmpl: (index, data) ->
    data = _(data).extend
      shop: @shop
    $(tmpl "tmpl_#{index}", data).appendTo @el

###
------------------------------------
###
App = ->
  $.ajax
    url: 'data/scenario.json'
    complete: (data) =>
      @data = data.responseJSON
      @game = new Game @data
$ ->
  window.app = new App()